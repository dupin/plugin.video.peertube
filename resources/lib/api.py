import urllib
import json

API_V1 = 'https://{host}/api/v1/{path}'
INSTANCES_PROVIDER = 'instances.joinpeertube.org'


def __load_datas(url):
    response = urllib.urlopen(url)
    charset = response.headers.getparam('charset')
    if charset is None:
        charset = 'utf-8'
    return json.loads(response.read().decode(charset))

def __peertube_query(host, path, params={}):
    url = API_V1.format(host=host, path=path)
    if params:
        url = url + '?' + __build_query_string(params)
    return __load_datas(url)


def __build_query_string(args):
    args = {k: v for k, v in args.items() if v} # Remove None items
    return urllib.urlencode(args)


# params are: start, count, sort, search
def load_list_of_instances(params={}):
    query_string = __build_query_string(params)
    url = 'https://{host}/api/v1/instances?{query}'.format(host=INSTANCES_PROVIDER, query=query_string)
    return __load_datas(url)


def load_instance_config(host):
    return __peertube_query(host, 'config')


# params are: category, count, sort, start
def load_list_of_videos(host, params={}):
    return __peertube_query(host, 'videos', params)


# params are: count, sort, start
def load_list_of_channels(host, params={}):
    return __peertube_query(host, 'video-channels', params)


def load_channel(host, name):
    return __peertube_query(host, 'video-channels/' + name)


def load_channel_list_of_videos(host, channel_uid, params):
    path = 'video-channels/' + channel_uid + '/videos'
    return __peertube_query(host, path, params)


def load_video(host, uuid):
    path='videos/' + uuid
    return __peertube_query(host, path)


def load_list_of_accounts(host, params):
    datas = __peertube_query(host, 'accounts', params)
    return datas


def load_account_videos(host, name):
    path = 'accounts/' + name + '/videos'
    datas = __peertube_query(host, path)
    return datas['data']
